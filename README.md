riscv-tools for shakti
======================

**Note: The main idea of maintaining this repository is to support Shakti classes of processors.**


This repository is a clone of original [riscv-tools](https://github.com/riscv/riscv-tools) repository.
Individual submodules in the original [riscv-tools](https://github.com/riscv/riscv-tools) repository are not updated.
Therefore, we have upgraded the versions of each submodule here to latest. As and when it is needed, we might pull the latest for each submodule
[riscv](https://github.com/riscv). This repository houses a set of RISC-V simulators, compilers, and other tools, including the following projects:

* [Spike](https://github.com/riscv/riscv-isa-sim/), the ISA simulator
* [riscv-tests](https://github.com/riscv/riscv-tests/), a battery of
ISA-level tests
* [riscv-opcodes](https://github.com/riscv/riscv-opcodes/), the
enumeration of all RISC-V opcodes executable by the simulator
* [riscv-pk](https://github.com/riscv/riscv-pk/), which contains `bbl`,
a boot loader for Linux and similar OS kernels, and `pk`, a proxy kernel that
services system calls for a target-machine application by forwarding them to
the host machine
* [riscv-fesvr](https://github.com/riscv/riscv-fesvr/), the host side of
a simulation tether that services system calls on behalf of a target machine

# <a name="quickstart"></a>Quickstart

    $ git clone "https://gitlab.com/faisalriyaz/riscv-tools.git"
	$ cd riscv-tools
	$ git submodule update --init --recursive
	$ export RISCV=<give the path to install riscv toolchain>
	$ ./build.sh


Example:

    $ git clone https://gitlab.com/faisalriyaz/riscv-tools.git"
	$ cd riscv-tools
	$ git submodule update --init --recursive
	$ export RISCV=/home/user/tools
    $ ./build.sh

Ubuntu packages needed:

	$ sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev

_Note:_ This requires a compiler with C++11 support (e.g. GCC >= 4.8).
To use a compiler different than the default, use:

	$ CC=gcc-5 CXX=g++-5 ./build.sh
